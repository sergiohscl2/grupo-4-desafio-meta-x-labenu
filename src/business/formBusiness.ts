import { IFormData} from "../model/interfaceData";
import { IdGenerator } from "../services/IdGenerator";
import { Authenticator } from "../services/Authenticator";
import { ROLES_TYPE } from "../types/leaguerTypes";
import { formInputDTO } from "../types/formInputDTO";
import Form from "../model/Form";


export class FormBusiness{
  private formData: IFormData ;
  private idGenerator: IdGenerator;
  private authenticator: Authenticator;
  
  constructor(formDatabase: IFormData){
    this.formData = formDatabase;
    this.idGenerator = new IdGenerator();
    this.authenticator = new Authenticator();
  }


  createForm = async (input: formInputDTO, token: string)=>{
    const {
      LEAGUER_id,
      project,
      time,
      type_hiring, 
      performance,
      comment_performance, 
      delivery_quality,
      comment_delivery_quality, 
      team_work,
      comment_team_work, 
      commitment,
      comment_commitment, 
      punctual,
      comment_punctual, 
      work_under_pressure,
      comment_work_under_pressure, 
      ceremonies,
      comment_ceremonies, 
      leadership_ability,
      comment_leadership_ability, 
      proactive,
      comment_proactive, 
      skills,
      comment_skills, 
      general_considerations, 
      main_features, 
      responsible, 
      compiled_evaluation, 
      SYSTEM_USER_id
    } = input;

    if(
      !project ||
      !time ||
      !type_hiring || 
      !performance ||
      !comment_performance || 
      !delivery_quality ||
      !comment_delivery_quality || 
      !team_work ||
      !comment_team_work || 
      !commitment ||
      !comment_commitment || 
      !punctual ||
      !comment_punctual || 
      !work_under_pressure ||
      !comment_work_under_pressure || 
      !ceremonies ||
      !comment_ceremonies || 
      !leadership_ability ||
      !comment_leadership_ability || 
      !proactive ||
      !comment_proactive || 
      !skills ||
      !comment_skills || 
      !general_considerations || 
      !main_features || 
      !responsible || 
      !compiled_evaluation
      ){
      throw new Error("Fill in all data");
      }

      const tokenData = this.authenticator.getTokenData(token)

        if (tokenData.role !== ROLES_TYPE.MENTOR && tokenData.role !== ROLES_TYPE.ADMINISTRATOR && tokenData.role !== ROLES_TYPE.MANAGER){
          throw new Error("You need to be a LEAGUER to register a Leaguer!");
        }

        const id = this.idGenerator.generateId();
        console.log(id)

      const form = new Form( 
        id,
        LEAGUER_id,
        project,
        time,
        type_hiring, 
        performance,
        comment_performance, 
        delivery_quality,
        comment_delivery_quality, 
        team_work,
        comment_team_work, 
        commitment,
        comment_commitment, 
        punctual,
        comment_punctual, 
        work_under_pressure,
        comment_work_under_pressure, 
        ceremonies,
        comment_ceremonies, 
        leadership_ability,
        comment_leadership_ability, 
        proactive,
        comment_proactive, 
        skills,
        comment_skills, 
        general_considerations, 
        main_features, 
        responsible, 
        compiled_evaluation, 
        SYSTEM_USER_id
      );

        await this.formData.insertForm(form)
        console.log(form)
  }


  searchFormById =  async (idForm: any): Promise<string> =>{

    if(!idForm){
      throw new Error("You need to use a valid id")
    }

    const checkLeaguer = await this.formData.findFormById(idForm)
    return checkLeaguer
  }

  // searchAllLeaguers =  async (token: string): Promise<string> =>{

  //     const tokenData = this.authenticator.getTokenData(token)

  //   if(!tokenData){
  //     throw new Error("You need to use a valid token")
  //   }

  //   const allLeaguers = await this.leaguerData.findAllLeaguer()
  //   return allLeaguers
  // }

  deleteForm = async (id: string, token: string): Promise<void> => {
    const verifiedToken = this.authenticator.getTokenData(token);
    console.log("oi to aquiiiiiiiiii",verifiedToken)

    if(verifiedToken.role !== ROLES_TYPE.MENTOR && verifiedToken.role !== ROLES_TYPE.ADMINISTRATOR){
      throw new Error("You need to be a MENTOR to delete a Leaguer!");
    }

    return await this.formData.deleteForm(id);
  }
}