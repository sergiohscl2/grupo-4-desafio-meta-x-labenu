import  app  from "./controller/app";
import { LeaguerBusiness } from "./business/leaguerBusiness";
import { LeaguerController } from "./controller/leaguerController";
import { LeaguerDatabase}  from "./data/leaguerDatabase";
import System_userBusiness from "./business/System_userBusiness";
import System_userController from "./controller/System_userController";
import { System_userData } from "./data/System_userData";
import { FormBusiness } from "./business/formBusiness";
import { FormDatabase } from "./data/formDatabase";
import { FormController } from "./controller/formController";
import { QuestionsBusiness } from "./business/questionsBusiness";
import { QuestionsController } from "./controller/questionsController";
import { QuestionsDatabase } from "./data/questionsDatabase";

const leaguerBusiness = new LeaguerBusiness(new LeaguerDatabase);
const leaguerController = new LeaguerController(leaguerBusiness);

const formBusiness = new FormBusiness(new FormDatabase);
const formController = new FormController(formBusiness);

const questionsBusiness = new QuestionsBusiness(new QuestionsDatabase);
const questionsController = new QuestionsController(questionsBusiness);

const system_userBusiness = new System_userBusiness(new System_userData());
const system_userController = new System_userController(system_userBusiness);


app.post("/questions/signup", questionsController.createQuestions);
app.delete("/questions/:id", questionsController.deleteQuestions);
app.get("/questions/:id", questionsController.searchQuestionsById);


app.post("/form/signup", formController.createForm);
app.delete("/form/:id", formController.deleteForm);
app.get("/form/:id", formController.searchFormById);

app.delete("/profile/:id", leaguerController.deleteLeaguer);
app.post("/leaguer/signup", leaguerController.createLeaguer);
app.get("/profile/:id", leaguerController.searchLeaguerById);
app.get("/leaguer", leaguerController.searchAllLeaguers);
app.put("/profile/:id", leaguerController.editLeaguer);

app.post("/signup", system_userController.signup);
app.post("/login", system_userController.login);




