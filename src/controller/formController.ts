import { Request, Response } from "express";
import { FormBusiness } from "../business/formBusiness";
import { formInputDTO } from "../types/formInputDTO";

export class FormController{
  constructor(
    private formBusiness: FormBusiness){}
  
  createForm = async (req: Request, res: Response): Promise<void> => {


    const input: formInputDTO  = {
      LEAGUER_id: req?.body.LEAGUER_id,
      project: req?.body.project,
      time: req.body?.time,
      type_hiring: req?.body.type_hiring, 
      performance: req?.body.performance,
      comment_performance: req?.body.comment_performance, 
      delivery_quality: req?.body.delivery_quality,
      comment_delivery_quality: req?.body.comment_delivery_quality, 
      team_work: req?.body.team_work,
      comment_team_work: req?.body.comment_team_work, 
      commitment: req?.body.commitment,
      comment_commitment: req?.body.comment_commitment, 
      punctual: req?.body.punctual,
      comment_punctual: req?.body.comment_punctual, 
      work_under_pressure: req?.body.work_under_pressure,
      comment_work_under_pressure: req?.body.comment_work_under_pressure, 
      ceremonies: req?.body.ceremonies,
      comment_ceremonies: req?.body.comment_ceremonies, 
      leadership_ability: req?.body.leadership_ability,
      comment_leadership_ability: req?.body.comment_leadership_ability, 
      proactive: req?.body.proactive,
      comment_proactive: req?.body.comment_proactive, 
      skills: req?.body.skills,
      comment_skills: req?.body.comment_skills, 
      general_considerations: req?.body.general_considerations, 
      main_features: req?.body.main_features, 
      responsible: req?.body.responsible, 
      compiled_evaluation: req?.body.compiled_evaluation, 
      SYSTEM_USER_id: req.body.SYSTEM_USER_id
    } 

          try{ 
            const token = req.headers.authorization as string
        

          const form = await this.formBusiness.createForm(input, token)
                console.log(form)

          res.status(201).send({ message:'Form create success'});
        } catch(error: any){
            switch(error.message){
              case "Form not found!":
                res.status(404).send(error.message)
              break 
              case "All data must be filled!":
                res.status(422).send(error.message)
              break
              case "Some a error!":
                res.status(500).send(error.message)
              default:
                res.status(400).send({message: error.message});
            }
        }
    }

    searchFormById =   async (req: Request, res: Response) =>{
      try{
        const idForm = req.params.id as string

        const form = await this.formBusiness.searchFormById(idForm)

        res.status(200).send({"leaguer": form})
        
      }catch(error: any){
          switch(error.message){
            case "ID Form not found!":
              res.status(404).send(error.message)
            break 
            case "Some a error!":
              res.status(500).send(error.message)
            default:
              res.status(400).send({message: error.message});
          }
      }
    }

//     searchAllLeaguers =  async (req: Request, res: Response) =>{
// try{
//       const token = req.headers.authorization as string;
//       const leaguer = await this.leaguerBusiness.searchAllLeaguers(token);
    
//       res.status(200).send(leaguer);
//     }catch(error: any){
//       switch(error.message){
//         case "Some a error!":
//           res.status(500).send(error.message)
//         default:
//           res.status(400).send({message: error.message});
//       }
//     }
//   }

  deleteForm = async (req: Request, res: Response) =>{
    try{
    const token: string = req.headers.authorization as string;
    const id: string = req.params.id as string;

      await this.formBusiness.deleteForm(id, token);

      res.status(201).send({ message: "Form delete success"});
    }
    catch (error: any) {
      res.status(400).send({ error: error.message });
    }
  }
}