import { Response, Request } from "express";
import System_userBusiness from "../business/System_userBusiness";
import { SystemInputDTO } from "../types/SystemInputDTO";

export default class System_userController {
  constructor(private system_userBusiness: System_userBusiness) {}

  signup = async (req: Request, res: Response) => {
    const { name, email, password, role } = req.body;

    const input: SystemInputDTO = {
      name,
      email,
      password,
      role,
    };
    try {
      const token = await this.system_userBusiness.signup(input);

      res.status(201).send({ message: "User registered successfully!", token});
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro no signup");
    }
  };

  login = async (req: Request, res: Response) => {
    const { name, email, password } = req.body;

    const input: SystemInputDTO = {
      name,
      email,
      password
    };
    try {
      const token = await this.system_userBusiness.login(input);
      res.status(201).send({ message: "User logged in successfully", token });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro no login");
    }
  };
}
