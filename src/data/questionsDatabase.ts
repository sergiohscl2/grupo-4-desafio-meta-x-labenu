import { BaseDatabase } from "./Basedatabase";
import { IQuestionsData } from "../model/interfaceData"
import Questions from '../model/Questions';

export class QuestionsDatabase extends BaseDatabase implements IQuestionsData{
  protected TABLE_NAME = "QUESTIONS"

  insertQuestions =  async (questions: Questions): Promise<void> => {
    try{
      await this.connection(this.TABLE_NAME).insert(questions)
    console.log(questions)
    } catch(error: any){
      throw new Error(error.message)
    }
  }

  findByQuestions = async (questions: string): Promise<string>=>{
    try {
        const question = await this.connection(this.TABLE_NAME)
        .select("*")
        .where({ questions })
    return question[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }

  findQuestionById = async (id: string): Promise<string>=>{
    try {
        const idQuestions = await this.connection(this.TABLE_NAME)
        .select("*")
        .where({ id })
        console.log(idQuestions[0])
      return idQuestions[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }


  deleteQuestions = async (id: string)=>{
    try {
      const deleteLeaguer = await this.connection(this.TABLE_NAME)
      .delete('id')
      .where({ id })
      return deleteLeaguer
    } catch (error: any) {
        throw new Error(error.message);
    }
  }
}