import Leaguer from '../model/Leaguer'
import { BaseDatabase } from "./Basedatabase";
import { ILeaguerData } from "../model/interfaceData"


export class LeaguerDatabase extends BaseDatabase implements ILeaguerData{
  protected TABLE_NAME = "PERFIL_LEAGUER"

  insertLeaguer =  async (leaguer: Leaguer): Promise<void> => {
    try{
      await this.connection(this.TABLE_NAME).insert({
        id: leaguer.id,
        name: leaguer.name,
        email: leaguer.email, 
        photo: leaguer.photo,
        team: leaguer.team,
        language: leaguer.language,
        responsible: leaguer.responsible,
        profession: leaguer.profession,
        technology: leaguer.technology, 
        SYSTEM_USER_id: leaguer.SYSTEM_USER_id,
        role: leaguer.role,
        stage: leaguer.stage
      })
    console.log(leaguer)
    } catch(error: any){
      throw new Error(error.message)
    }
  }


  findLeaguerByEmail = async (email: string): Promise<Leaguer>=>{
    try {
        const emailLeaguer = await this.connection(this.TABLE_NAME)
        .select('*')
        .where({ email })
        console.log(emailLeaguer)
      return emailLeaguer[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }


  findLeaguerById = async (id: string): Promise<string>=>{
    try {
        const leaguerTypes = await this.connection(this.TABLE_NAME)
        .select("*")
        .where({ id })
        console.log(leaguerTypes[0])
      return leaguerTypes[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }
  

  findAllLeaguer = async ():Promise<any>=>{
    try {
        const allLeaguers = await this.connection(this.TABLE_NAME)
        .select("*")
        console.log(allLeaguers[0])
      return allLeaguers[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }

  findLeaguerByName = async (name: string): Promise<string>=>{
    try {
        const nameLeaguer = await this.connection(this.TABLE_NAME)
        .select("*")
        .where({ name })
    return nameLeaguer[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }

  deleteLeaguer = async (id: string)=>{
    try {
      const deleteLeaguer = await this.connection(this.TABLE_NAME)
      .delete('id')
      .where({ id })
      return deleteLeaguer
    } catch (error: any) {
      throw new Error(error.message);
    }
  }

  editLeaguers= async (leaguer: Leaguer): Promise<any> => {
    try {
      await this.connection(this.TABLE_NAME)
      .where('id')
      .update({
        id: leaguer.id,
        name: leaguer.name,
        email: leaguer.email, 
        photo: leaguer.photo,
        team: leaguer.team,
        language: leaguer.language,
        responsible: leaguer.responsible,
        profession: leaguer.profession,
        technology: leaguer.technology, 
        SYSTEM_USER_id: leaguer.SYSTEM_USER_id,
        role: leaguer.role,
        stage: leaguer.stage
      })
  } catch (error: any) {
    throw new Error(error.message);
    }
  }
}

//UPDATE PERFIL_LEAGUER SET `name` = "Diego Macêdo", `stage` = "LAB" WHERE `id` = "9375ecae-fd1d-4221-90d7-d3a5866b1768";