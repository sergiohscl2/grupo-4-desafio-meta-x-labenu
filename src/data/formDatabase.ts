import Form from '../model/Form'
import { BaseDatabase } from "./Basedatabase";
import { IFormData} from "../model/interfaceData"

export class FormDatabase extends BaseDatabase implements IFormData{
  protected TABLE_NAME = "FORM"

  insertForm =  async (form: Form): Promise<void> => {
    try{
      await this.connection(this.TABLE_NAME).insert({
      id: form.id,
      LEAGUER_id: form.LEAGUER_id,
      project: form.project,
      time: form.time,
      type_hiring: form.type_hiring, 
      performance: form.performance,
      comment_performance: form.comment_performance, 
      delivery_quality: form.delivery_quality,
      comment_delivery_quality: form.comment_delivery_quality, 
      team_work: form.team_work,
      comment_team_work: form.comment_team_work, 
      commitment: form.commitment,
      comment_commitment: form.comment_commitment, 
      punctual: form.punctual,
      comment_punctual: form.comment_punctual, 
      work_under_pressure: form.work_under_pressure,
      comment_work_under_pressure: form.comment_work_under_pressure, 
      ceremonies: form.ceremonies,
      comment_ceremonies: form.comment_ceremonies, 
      leadership_ability: form.leadership_ability,
      comment_leadership_ability: form.comment_leadership_ability, 
      proactive: form.proactive,
      comment_proactive: form.comment_proactive, 
      skills: form.skills,
      comment_skills: form.comment_skills, 
      general_considerations: form.general_considerations, 
      main_features: form.main_features, 
      responsible: form.responsible, 
      compiled_evaluation: form.compiled_evaluation, 
      SYSTEM_USER_id: form.SYSTEM_USER_id
    })
    console.log(form)
    } catch(error: any){
      throw new Error(error.message)
    }
  }

  deleteForm = async (id: string)=>{
    try {
      const deleteForms = await this.connection(this.TABLE_NAME)
      .delete('id')
      .where({ id })
      return deleteForms
    } catch (error: any) {
      throw new Error(error.message);
    }
  }

  findFormById = async (id: string): Promise<string>=>{
    try {
        const idForm = await this.connection(this.TABLE_NAME)
        .select("*")
        .where({ id })
        console.log(idForm[0])
      return idForm[0];
    } catch (error: any) {
      throw new Error(error.message);
    }
  }
}