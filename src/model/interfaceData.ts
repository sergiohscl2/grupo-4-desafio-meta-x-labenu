import Form from "./Form"
import Leaguer from "./Leaguer"
import Questions from "./Questions"
import System_user from "./system_user"

export interface ILeaguerData{
  insertLeaguer(leaguer: Leaguer): Promise<void>
  findLeaguerByEmail(email: string): Promise<any>
  findLeaguerById(id: string): Promise<string>
  findLeaguerByName(name: string): Promise<string>
  findAllLeaguer(): Promise<any>
  deleteLeaguer(id: string): Promise<any>
  editLeaguers(leaguer: Leaguer): Promise<any>  
}

export interface IFormData{
  insertForm(form: Form): Promise<void>
  deleteForm(id: string): Promise<any> 
  findFormById(id: string): Promise<string>

}
export interface ISystem_userData{
  createSystem_user(system: System_user): Promise<System_user>
  findSystem_userByEmail(email: string):Promise<System_user>
}

export interface IQuestionsData{
  insertQuestions(questions: Questions): Promise<void>
  findByQuestions(questions: string): Promise<string>
  deleteQuestions(id: string): Promise<any>
  findQuestionById(id: string): Promise<string>
  }

