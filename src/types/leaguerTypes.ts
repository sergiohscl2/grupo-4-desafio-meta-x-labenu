
export enum ROLES_TYPE{
  ADMINISTRATOR = "ADMINISTRATOR",
  MENTOR =   "MENTOR",
  MANAGER = "MANAGER",
  LEAGUER = "LEAGUER"
}

export enum ROLES_STAGE{
  INTRODUCTION = "INTRODUCTION",
  LAB =   "LAB",
  BETA = "BETA"
}

export type authenticationData = {
  id: string    
}

export type FindByEmailResponse = {
  id: string;
  name: string;
  email: string;
}[];



