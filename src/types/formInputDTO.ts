import { ROLES_EVALUATION, ROLES_TYPE_HIRING } from "./formTypes"

export type formInputDTO = {
  LEAGUER_id: string
  project: string
  time: string
  type_hiring?: ROLES_TYPE_HIRING
  performance?: ROLES_EVALUATION
  comment_performance?: string
  delivery_quality?: ROLES_EVALUATION
  comment_delivery_quality?: string
  team_work?: ROLES_EVALUATION
  comment_team_work?: string
  commitment?: ROLES_EVALUATION
  comment_commitment?: string
  punctual?: ROLES_EVALUATION
  comment_punctual?: string
  work_under_pressure?: ROLES_EVALUATION
  comment_work_under_pressure?: string
  ceremonies?:ROLES_EVALUATION
  comment_ceremonies?: string
  leadership_ability?: ROLES_EVALUATION
  comment_leadership_ability?: string
  proactive?: ROLES_EVALUATION
  comment_proactive?: string
  skills?: ROLES_EVALUATION
  comment_skills?: string
  general_considerations?: string
  main_features?: string
  responsible?: string
  compiled_evaluation?: string
  SYSTEM_USER_id: string
}