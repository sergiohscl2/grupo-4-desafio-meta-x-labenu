export type QuestionsInputDTO = {
  questions: string,
  answer: string,
  SYSTEM_USER_id: string, 
}