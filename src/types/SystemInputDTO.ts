import { ROLES_TYPE } from "./leaguerTypes"

export type SystemInputDTO = {
    name: string
    email: string
    password: string
    role?: ROLES_TYPE
}